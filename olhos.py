from sys import argv, exit
from random import random
import numpy as np
import cv2

if len(argv) > 1:
    filename = argv[1]
else:
    filename = 'data/rostos.jpg'

# varios para download: https://github.com/Itseez/opencv/tree/master/data/haarcascades
rostos_classificador = cv2.CascadeClassifier('data/rostos.xml')
olhos_classificador = cv2.CascadeClassifier('data/olhos.xml')
colorida = cv2.imread(filename)
cinza = cv2.cvtColor(colorida, cv2.COLOR_BGR2GRAY)

cv2.imshow('Detector de olhos',colorida)
cv2.waitKey(0)

cv2.imshow('Detector de olhos',cinza)
cv2.waitKey(0)

#tranforma a imagem cinza para o formato BGR para poder desenhar os quadrados
colorida = cv2.cvtColor(cinza, cv2.COLOR_GRAY2BGR)

#Buscar rostos:
rostos = rostos_classificador.detectMultiScale(cinza)
for r in rostos:
    x0,y0,w,h = r
    x1 = x0 + w
    y1 = y0 + h
    r = int(random()*255)
    g = int(random()*255)
    b = int(random()*255)
    cv2.rectangle(colorida, (x0,y0), (x1,y1), (b,g,r), 4)

cv2.imshow('Detector de olhos',colorida)
cv2.waitKey(0)

#Buscar olhos:
olhos = olhos_classificador.detectMultiScale(cinza)
for o in olhos:
    x0,y0,w,h = o
    x1 = x0 + w
    y1 = y0 + h
    r = int(random()*255)
    g = int(random()*255)
    b = int(random()*255)
    cv2.rectangle(colorida, (x0,y0), (x1,y1), (b,g,r), 2)

cv2.imshow('Detector de olhos',colorida)
cv2.waitKey(0)

cv2.destroyAllWindows()
